<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'first_name'    => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'dob'   => $this->faker->dateTimeBetween('1990-01-01', '2012-12-31')->format('Y-m-d'),//'1996-11-28',//$this->faker->dateTimeBetween('1990-01-01', '2012-12-31')->format('d/m/Y'),
            'address'   => $this->faker->address(),
            'phone_number'  => '0123456789',
            'username' => $this->faker->unique()->userName(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        ];
    }

    public function createNewUserData()
    {
        return [
            'first_name'    => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'dob'   => $this->faker->dateTimeBetween('1990-01-01', '2012-12-31')->format('Y-m-d'),//'1996-11-28',//$this->faker->dateTimeBetween('1990-01-01', '2012-12-31')->format('d/m/Y'),
            'address'   => $this->faker->address(),
            'phone_number'  => '0123456789',
            'username' => $this->faker->unique()->userName(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        ];
    }

}
