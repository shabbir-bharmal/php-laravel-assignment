**About Assignment**

- Created the assignment using Laravel, a PHP framework.
- Created a simple login screen
- Created a simple registration screen
- Used MySQL database

---

## How to use

1. Clone the repository with git clone
2. Copy .env.example file to .env and edit database credentials there.
3. Run composer install.
4. Run php artisan key:generate
5. Run npm install
6. Run npm run dev
7. Run php artisan migrate

That's it: launch the main URL {url}.
You can register and login

---

## About the Test case

1. Used PHP/Laravel's inbuilt php unit for writing test cases.
2. Test cases are written under /tests/Feature directory, for login LoginTest.php and for register, RegistrationTest.php.
3. Written the test cases on High level for now, but if require, can breakdown the entire case and can write the test cases on individual field as well.
4. To run the test cases, run php artisan test
5. To run the individual test case, run ./vendor/bin/phpunit --filter LoginTest


---